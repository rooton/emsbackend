# Ems Backend

## Development backend server

This project use MySQL as database.
Before run this development server you should instal/run Mysql and create database (db_ems) and put your db autorization data to 

Location: src\main\resources\application.properties`


`spring.datasource.url=jdbc:mysql://localhost:3306/db_ems`  
`spring.datasource.username=<your_db_login>`  
`spring.datasource.password=<your_db_pass>`  


Import project to your prefered IDE.
When maven finish to download packages yo can run project (tomcat included in `spring-boot-starter-parent`).

When server start and run you can send base RESTfull reguests `http://localhost:8080/` (`spring-boot-starter-data-rest`).
After that you can download and run frontend development server (https://bitbucket.org/rooton/emsfrontend/)