package com.example.ems;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.example.ems.models.Department;
import com.example.ems.models.Employee;
import com.example.ems.models.Position;
import com.example.ems.repositories.DepartmentRepository;
import com.example.ems.repositories.EmployeeRepository;
import com.example.ems.repositories.PositionRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = EmsApplication.class)
@AutoConfigureMockMvc
@WebAppConfiguration
public class EmsApplicationTests {
	
	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));
	
	private MockMvc mockMvc;

	@Autowired
    private PositionRepository positionRepository;
	@Autowired
    private DepartmentRepository departmentRepository;
	@Autowired
    private EmployeeRepository employeeRepository;
	
	@Autowired
	ObjectMapper objectMapper;
	
	private List<Position> positionsList = new ArrayList<>();
	private List<Department> departmentsList = new ArrayList<>();
	private List<Employee> employeesList = new ArrayList<>();
	
	@Autowired
    private WebApplicationContext webApplicationContext;
	
	
	@Before
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
        
        this.positionRepository.deleteAllInBatch();
        this.departmentRepository.deleteAllInBatch();
        this.employeeRepository.deleteAllInBatch();
                  
        Position positionIT = new Position();
        positionIT.setName("Software Developer");
               
        Position positionHR = new Position();
        positionHR.setName("Loan specialist");
        
        this.positionsList.add(positionRepository.save(positionIT));
        this.positionsList.add(positionRepository.save(positionHR));

        Department departmentIT = new Department();
        departmentIT.setName("IT Department");
        departmentIT.setParentId(0);
        
        Department departmentHR = new Department();
        departmentHR.setName("HR Department");
        departmentHR.setParentId(0);
        
        this.departmentsList.add(departmentRepository.save(departmentIT));
        this.departmentsList.add(departmentRepository.save(departmentHR));
        
        Employee employeeIT = new Employee();
        employeeIT.setName("Test");
        employeeIT.setSurname("Tester");
        employeeIT.setEmail("test1@email.com");
        employeeIT.setBirthDate(new Date());
        employeeIT.setEmploymentDate(new Date());
        employeeIT.setDepartmentId(0);
        
        Employee employeeHR = new Employee();
        employeeHR.setName("Gulaga");
        employeeHR.setSurname("Mammadov");
        employeeHR.setEmail("test@email.com");
        employeeHR.setBirthDate(new Date());
        employeeHR.setEmploymentDate(new Date());
        employeeHR.setDepartmentId(1);        
       
        this.employeesList.add(employeeRepository.save(employeeIT));
        this.employeesList.add(employeeRepository.save(employeeHR));
	}
	
		
	@Test
    public void getPosition() throws Exception {
		
		this.mockMvc.perform(get("/api/position")
			 	.accept(MediaType.APPLICATION_JSON_UTF8))
		        .andExpect(status().isOk())
		        .andExpect(content().contentType(contentType))
		        /*.andExpect(jsonPath("$[0].id", is(this.departmentList.get(0).getId().intValue())))
		        .andExpect(jsonPath("$[0].name", is(this.departmentList.get(0).getName())))
		        .andExpect(jsonPath("$[0].parentId", is(this.departmentList.get(0).getParentId().intValue())))*/;
    }
	
	
	@Test
    public void postPosition() throws Exception {
		
		Position position = new Position();
		position.setName("New Position");
		
		this.mockMvc.perform(post("/api/position")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(position)))
                .andExpect(status().isCreated());		 
    }
	
	
	@Test
    public void putPosition() throws Exception {
		
		Position position = new Position();
		position.setName("Put Position");
		
		Integer positionId = this.positionsList.get(0).getId();
				
		this.mockMvc.perform(put("/api/position/" + positionId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(position)))
                .andExpect(status().isNoContent());		 
    }
	
	@Test
    public void patchPosition() throws Exception {
		
		Position position = new Position();
		position.setName("Patch Position");
		
		Integer positionId = this.positionsList.get(1).getId();
				
		this.mockMvc.perform(patch("/api/position/" + positionId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(position)))
                .andExpect(status().isNoContent());		 
    }
	
	@Test
    public void deletePosition() throws Exception {
		
		Integer positionId = this.positionsList.get(1).getId();
				
		this.mockMvc.perform(delete("/api/position/" + positionId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());		 
    }
	
	//------------------------------Departments
	
	@Test
    public void getDepartment() throws Exception {
		
		this.mockMvc.perform(get("/api/department")
			 	.accept(MediaType.APPLICATION_JSON_UTF8))
		        .andExpect(status().isOk())
		        .andExpect(content().contentType(contentType));
    }
	
	
	@Test
    public void postDepartment() throws Exception {
		
		Department department = new Department();
		department.setName("New department");
		department.setParentId(0);
		
		this.mockMvc.perform(post("/api/department")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(department)))
                .andExpect(status().isCreated());		 
    }
	
	
	@Test
    public void putDepartment() throws Exception {
		
		Department department = new Department();
		department.setName("Put department");
		department.setParentId(1);
		
		Integer positionId = this.departmentsList.get(0).getId();
				
		this.mockMvc.perform(put("/api/department/" + positionId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(department)))
                .andExpect(status().isNoContent());		 
    }
	
	@Test
    public void patchDepartment() throws Exception {
		
		Department department = new Department();
		department.setParentId(0);
		
		Integer positionId = this.departmentsList.get(0).getId();
				
		this.mockMvc.perform(patch("/api/department/" + positionId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(department)))
                .andExpect(status().isNoContent());		 
    }
	
	@Test
    public void deleteDepartment() throws Exception {
				
		Integer positionId = this.departmentsList.get(1).getId();
				
		this.mockMvc.perform(delete("/api/department/" + positionId)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNoContent());		 
    }

	
	//------------------------------Employees
	
		@Test
	    public void getEmployee() throws Exception {
			
			this.mockMvc.perform(get("/api/employee")
				 	.accept(MediaType.APPLICATION_JSON_UTF8))
			        .andExpect(status().isOk())
			        .andExpect(content().contentType(contentType));
	    }
		
		
		@Test
	    public void postEmployee() throws Exception {
			
			Employee employee = new Employee();
			employee.setName("New Test");
			employee.setSurname("Surname Tester");
			employee.setEmail("test25@email.com");
			employee.setBirthDate(new Date());
			employee.setEmploymentDate(new Date());
			employee.setDepartmentId(0);
			
			this.mockMvc.perform(post("/api/employee")
	                .contentType(MediaType.APPLICATION_JSON)
	                .content(objectMapper.writeValueAsString(employee)))
	                .andExpect(status().isCreated());		 
	    }
		
		
		@Test
	    public void putEmployee() throws Exception {
			
			Employee employee = new Employee();
			employee.setName("New Put name");
			employee.setSurname("Surname Tester");
			employee.setEmail("test25@email.com");
			employee.setBirthDate(new Date());
			employee.setEmploymentDate(new Date());
			employee.setDepartmentId(0);
			
			Integer positionId = this.employeesList.get(0).getId();
					
			this.mockMvc.perform(put("/api/employee/" + positionId)
	                .contentType(MediaType.APPLICATION_JSON)
	                .content(objectMapper.writeValueAsString(employee)))
	                .andExpect(status().isNoContent());		 
	    }
		
		@Test
	    public void patchEmployee() throws Exception {
			
			Employee employee = new Employee();
			employee.setName("New patch name");
			employee.setSurname("Surname patch");
			employee.setEmploymentDate(new Date());
			employee.setDepartmentId(0);
			
			Integer positionId = this.employeesList.get(0).getId();
					
			this.mockMvc.perform(patch("/api/employee/" + positionId)
	                .contentType(MediaType.APPLICATION_JSON)
	                .content(objectMapper.writeValueAsString(employee)))
	                .andExpect(status().isNoContent());		 
	    }
		
		@Test
	    public void deleteEmployee() throws Exception {
					
			Integer positionId = this.employeesList.get(1).getId();
					
			this.mockMvc.perform(delete("/api/employee/" + positionId)
	                .contentType(MediaType.APPLICATION_JSON))
	                .andExpect(status().isNoContent());		 
	    }
	
	
}