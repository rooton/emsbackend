package com.example.ems.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.example.ems.models.Employee;

@RepositoryRestResource (collectionResourceRel = "employee", path="employee")
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {	
}
