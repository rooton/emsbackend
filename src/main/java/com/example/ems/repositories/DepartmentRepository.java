package com.example.ems.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.example.ems.models.Department;

@RepositoryRestResource (collectionResourceRel = "department", path="department")
public interface DepartmentRepository extends JpaRepository<Department, Integer> {
}