package com.example.ems.utils;

import java.net.URI;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import org.springframework.data.rest.webmvc.config.RepositoryRestMvcConfiguration;

import com.example.ems.models.Department;
import com.example.ems.models.Employee;
import com.example.ems.models.Position;

@Configuration
public class RepositoryConfig extends
        RepositoryRestMvcConfiguration {
    
	private static final String MY_BASE_URI_URI = "/api";
	
    @Bean
    public RepositoryRestConfigurer repositoryRestConfigurer() {

        return new RepositoryRestConfigurerAdapter() {
            @Override
            public void configureRepositoryRestConfiguration(
                                 RepositoryRestConfiguration config) {
            	 //config.setDefaultMediaType(MediaType.APPLICATION_JSON_UTF8);
            	
            	 config.setBasePath(MY_BASE_URI_URI);
            	 config.exposeIdsFor(Position.class, Department.class, Employee.class );
            }
        };
    }
}